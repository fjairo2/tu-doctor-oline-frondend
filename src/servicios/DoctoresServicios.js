import axios from "axios";

const DoctoresServicios = {};

DoctoresServicios.listarDoctores = () => {
    return axios.get("http://localhost:8080/Doctores");
}

DoctoresServicios.buscarDoctores = (criterio) => {
    return axios.get("http://localhost:8080/doctores?q=" + criterio);
}

DoctoresServicios.buscardoctor = (id) => {
    return axios.get("http://localhost:8080/doctores/" + id);
}

DoctoresServicios.guardarDoctor = (doctor) => {
    return axios.post("http://localhost:8080/doctores", doctor);
}

DoctoresServicios.modificarDoctor = (id, doctor) => {
    return axios.put("http://localhost:8080/doctores/" + id, doctor);
}

DoctoresServicios.borrarDoctor = (id)=>{
    return axios.delete("http://localhost:8080/doctores/" + id);
}

export default DoctoresServicios;