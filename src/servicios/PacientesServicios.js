import axios from "axios";

const PacientesServicios = {};

PacientesServicios.listarPacientes = () => {
    return axios.get("http://localhost:8080/pacientes");
}

PacientesServicios.buscarPacientes = (criterio) => {
    return axios.get("http://localhost:8080/pacientes?q=" + criterio);
}

PacientesServicios.buscarPaciente = (id) => {
    return axios.get("http://localhost:8080/pacientes/" + id);
}

PacientesServicios.guardarPaciente = (paciente) => {
    return axios.post("http://localhost:8080/pacientes", paciente);
}

PacientesServicios.modificarPaciente = (id, paciente) => {
    return axios.put("http://localhost:8080/pacientes/" + id, paciente);
}

PacientesServicios.borrarPaciente = (id) => {
    return axios.delete("http://localhost:8080/pacientes/" + id);
}
export default PacientesServicios;