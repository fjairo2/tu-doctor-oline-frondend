import axios from "axios";

const CitasServicios={};

CitasServicios.listarCitas = () => {
    return axios.get("http://localhost:8080/citas");
}

CitasServicios.buscarCitas = (criterio) => {
    return axios.get("http://localhost:8080/citas?q=" + criterio);
}

CitasServicios.buscarCita = (id) => {
    return axios.get("http://localhost:8080/citas/" + id);
}

CitasServicios.guardarCita = (cita) => {
    return axios.post("http://localhost:8080/citas", cita);
}

CitasServicios.modificarCita = (id, cita) => {
    return axios.put("http://localhost:8080/citas/" + id, cita);
}

CitasServicios.borrarCita = (id)=>{
    return axios.delete("http://localhost:8080/citas/" + id);
}

export default CitasServicios;