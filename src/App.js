import Header from "./componentes/general/Header";
import FormPacientes from "./componentes/pacientes/FormPacientes";
import ListadoDoctores from "./componentes/doctores/ListadoDoctores";
import FormDoctores from "./componentes/doctores/FormDoctores";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Bienvenida from "./componentes/general/Bienvenida";
import FormCitas from "./componentes/citas/FormCitas";
import ListadoCitas from "./componentes/citas/ListadoCitas";
import Login from "./componentes/general/Login";
import ListadoPacientes from "./componentes/pacientes/ListadoPacientes";
import { ContextoUsuario } from "./componentes/general/ContextoUsuario";
import { useState } from "react";

const App = () => {
  const [usuario, setUsuario] = useState({nombres: "", estadologin: 0});
  return (
    <div>
    <BrowserRouter>
    <ContextoUsuario.Provider value={{usuario, setUsuario}}>
    <Header />
    <Routes>
     <Route path="/" element={<Bienvenida />} exact></Route>
     <Route path="/pacientes" element={<ListadoPacientes />} exact></Route>
     <Route path="/pacientes/form" element={<FormPacientes />} exact></Route>
     <Route path="/pacientes/form/:id" element={<FormPacientes />} axact></Route>
     <Route path="/doctores" element={<ListadoDoctores/>} exact></Route>
     <Route path="/doctores/form" element={<FormDoctores/>} exact></Route>
     <Route path="/doctores/form/:id" element={<FormDoctores/>} exact></Route>
     <Route path="/citas" element={<ListadoCitas/>} exact></Route>
     <Route path="/citas/form" element={<FormCitas/>} exact></Route>
     <Route path="/citas/form/:id" element={<FormCitas/>} exact></Route>
     <Route path="/login" element={<Login/>} exact></Route> 
     </Routes>
     </ContextoUsuario.Provider>
    </BrowserRouter>
    </div>
  );
}

export default App;
