const EstadoLogin = {
    NO_LOGUEADO: 0,
    ADMIN: 1,
    CLIENTE: 2,
    DOCTOR: 3,
    NUEVO: 4

}

export default EstadoLogin;