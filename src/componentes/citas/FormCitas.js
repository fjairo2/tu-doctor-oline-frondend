import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import CitasServicios from "../../servicios/CitasServicios";



const FormCitas = () => {
    const { id } = useParams();
    const navegar = useNavigate();

    const [nombres, setNombres] = useState("");
    const [apellidos, setApellidos] = useState("");
    const [documento, setDocumento] = useState("");
    const [tipo, setTipo] = useState("");
    const [fecha, setFecha] = useState("");
    const [hora, setHora] = useState("");
    const [mensaje, setMensaje] = useState("");
    const [titulo, setTitulo] = useState("");

    const guardarCita = async (event) => {
        event.preventDefault();
        try {
            const cita = {
                nombres: nombres,
                apellidos: apellidos,
                documento: documento,
                tipo: tipo,
                fecha: fecha,
                hora: hora,
            }
            console.log(cita);
            if (id == null) {
                await CitasServicios.guardarCita(cita);
                navegar("/");
            } else {
                await CitasServicios.modificarCita(id, cita);
                navegar("/citas");
            }
        } catch (error) {
            setMensaje("Ocurrió un error")
        }
    }

    const cargarCita = async () => {
        try {
            if (id != null) {
                const respuesta = await CitasServicios.buscarCita(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data)
                    setNombres(respuesta.data.nombres);
                    setApellidos(respuesta.data.apellidos);
                    setDocumento(respuesta.data.documento);
                    setTipo(respuesta.data.tipo);
                    setFecha(respuesta.data.fecha);
                    setHora(respuesta.data.hora);
                }
                setTitulo("Edición");
            } else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error")
        }
    }

    const cancelar = () => {
        if (id != null) {
            navegar("/citas");
        } else {
            navegar("/");
        }
    }
    useEffect(() => {
        cargarCita(id);
    }, [])

    const cambiarNombres = (event) => {
        setNombres(event.target.value);
    };
    const cambiarApellidos = (event) => {
        setApellidos(event.target.value);
    };
    const cambiarDocumento = (event) => {
        setDocumento(event.target.value);
    };
    const cambiarTipo = (event) => {
        setTipo(event.target.value);
    };
    const cambiarFecha = (event) => {
        setFecha(event.target.value);
    };
    const cambiarHora = (event) => {
        setHora(event.target.value);
    };

    return (<div className="container mt-3">
        <h3>{titulo} de citas</h3>
        <form className="container" action="">
            <div className="row">
                <div className="col-4">
                    <label htmlFor="nombres">Nombres *</label>
                    <input className="form-control" onChange={cambiarNombres} value={nombres} type="text" name="nombres" id="nombres" />
                </div>
                <div className="col-4">
                    <label htmlFor="apellidos">apellidos *</label>
                    <input className="form-control" onChange={cambiarApellidos} value={apellidos} type="text" name="apellidos" id="apellidos" />
                </div>
                <div className="col-4">
                    <label htmlFor="documento">Ingrese documento *</label>
                    <input className="form-control" onChange={cambiarDocumento} readOnly={id != null ? true : false} value={documento} type="number" name="documento" id="documento" />
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-4">
                    <label htmlFor="tipo">tipo de cita *</label>
                    <select className="form-select" onChange={cambiarTipo} type="text" name="tipo" id="tipo" required >
                        <option value="" defaultValue>Seleccione tipo</option>
                        <option value="Cédula">Medico general</option>
                        <option value="Cédula de extranjería">especialista</option>
                    </select>
                </div>
                <div className="col-4">
                    <label htmlFor="fecha">Ingrese fecha</label>
                    <input className="form-control" onChange={cambiarFecha} value={fecha} type="date" name="fecha" id="fecha" />
                </div>
                <div className="col-4">
                    <label htmlFor="fecha">Ingrese hora</label>
                    <input className="form-control" onChange={cambiarHora} value={hora} type="date" name="fecha" id="fecha" />
                </div>
            </div>
            <div className="mt-3">
                <button onClick={guardarCita} className="btn btn-primary me-2">Guardar</button>
                <button onClick={cancelar} type="button" className="btn btn-secondary">Cancelar</button>
                <div id="mensaje">{mensaje}
                </div>
            </div>
        </form>
    </div>

    )
}

export default FormCitas;