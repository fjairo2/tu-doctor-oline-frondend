const ListadoCitas= () => {
    return(
        <div className="container">
            <h3 className="mt-3">Gestión de citas</h3>
            <table className="table table-sm">
                <thead>
                    <tr>
                        <th>hora</th>
                        <th>facha</th>
                        <th>Nombre paciente</th>
                        <th>registro médico</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>10:30</td>
                            <td>31-10-2022</td>
                            <td>Juan Lozano</td>
                            <td>123WQ</td>
                            <td>
                                <button>Activo</button>
                                <button>Inactivo</button>
                            </td>

                        </tr>
                </tbody>
            </table>

        </div>
    )
}

export default ListadoCitas;