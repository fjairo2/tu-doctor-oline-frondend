import { useContext } from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import logo from "../../imgs/logoDoctorOnline.jpg";
import EstadoLogin from "../../enums/EstadoLogin";
import { ContextoUsuario } from "./ContextoUsuario";

const Header = () => {

  const navigateTo = useNavigate();
  const { usuario, setUsuario } = useContext(ContextoUsuario);

  const revisarSesion = () => {
    if (sessionStorage.getItem("estadologin") != null) {
      const usuarioSesion = {
        nombres: sessionStorage.getItem("nombres"),
        estadologin: parseInt(sessionStorage.getItem("estadologin"))
      }
      //console.log(usuarioSesion);
      setUsuario(usuarioSesion);
    }
    else {
      setUsuario({ nombres: "", estadologin: EstadoLogin.NO_LOGUEADO });
    }
  }
  const cerrarSesion = () => {
    sessionStorage.clear();
    revisarSesion();
    navigateTo("/");
  }


  useEffect(() => {
    revisarSesion();
  }, [])

  return (
    <nav className="navbar navbar-expand-lg navbar-dark fondo-nav sticky-top">
      <div className="container-fluid my-1">
        <a className="navbar-brand" href="/">
          <img src={logo} alt="" className="img-fluid" />
        </a>
        {
          usuario.estadologin === EstadoLogin.NO_LOGUEADO ? (
            <>

              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNavDropdown">
              </div>

              <div className="col-md-3 text-end">
                <a href="/login" type="button" className="btn btn-outline-primary me-2">Login</a>
                <a href="/pacientes/form" type="button" className="btn  btn-outline-primary">Sign-up </a>
              </div>
            </>
          ) :
            usuario.estadologin === EstadoLogin.ADMIN ? (
              <>
                <ul className="nav nav-pills">
                  <li className="nav-item"><a href="/" className="nav-link active" aria-current="page">Inicio</a></li>
                  <li className="nav-item"><a href="/pacientes" className="nav-link">Pacientes</a></li>
                  <li className="nav-item"><a href="/doctores" className="nav-link">Doctores</a></li>
                  <li className="nav-item"><a href="/citas" className="nav-link">Gestión de citas</a></li>
                  <li className="nav-item"><a href="/citas/form" className="nav-link">Pedir cita</a></li>
                </ul>

                <div className="col-md-4 text-end text-light">
                  {usuario.nombres}
                  <button onClick={cerrarSesion} type="button" className="btn btn-sm btn-warning ms-2">Cerrar sesion</button>
                </div>
              </>
            ) : usuario.estadologin === EstadoLogin.DOCTOR ? (
              <>
                <ul className="nav nav-pills">
                  <li className="nav-item"><a href="/citas" className="nav-link">Gestión de citas</a></li>
                </ul>

                <div className="col-md-4 text-end text-light">
                  {usuario.nombres}
                  <button onClick={cerrarSesion} type="button" className="btn btn-sm btn-warning mx-3">Cerrar sesion</button>
                </div>

              </>
            ) : (
              <>
                <ul className="nav nav-pills">
                  <li className="nav-item"><a href="/citas/form" className="nav-link">Pedir cita</a></li>
                </ul>

                <div className="col-md-4 text-end text-light">
                  {usuario.nombres}
                  <button onClick={cerrarSesion} type="button" className="btn btn-sm btn-warning mx-5">Cerrar sesion</button>
                </div>

              </>
            )}
      </div>
    </nav>
  )
}
export default Header;