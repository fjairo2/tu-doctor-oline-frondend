import img2 from "../../imgs/personas.jpg";
import imgGaleria1 from "../../imgs/edificio.jpeg";
import imgGaleria2 from "../../imgs/silla.jpeg";
import imgGaleria3 from "../../imgs/mujer.jpeg";
import imgGaleria4 from "../../imgs/flor.jpeg";

import estilos from "../../css/estilos.css";


const Bienvenida = () => {
  return (
    <main>
      <section className=" text-center" id="inicio">
        <div className=" pb-5 jumbotron">
          <h1 className="display-5 fw-bold mt-5">Jumbotron personalizado</h1>
          <p className="col-md-8 fs-4 mx-auto mt-5">Usando una serie de utilidades, puede crear este jumbotron, como el de versiones
            anteriores de Bootstrap. Echa un vistazo a los ejemplos a continuación para saber cómo puedes remezclarlo y
            cambiarle el estilo a tu gusto.</p>
          <button className="btn btn-primary btn-lg" type="button">Más Información</button>
        </div>
      </section>

      <section id="intro">
        <div className="container">
          <div className="row mt-5">
            <div className="col-sm-12 col-md-6">
              <h2 className="mt-5">Nuestra atención es realmente excepcional </h2>
              <p className="mt-5">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Pariatur explicabo numquam minima
                laboriosam fuga recusandae dolor placeat rerum, harum Lorem, ipsum dolor sit amet consectetur adipisicing
                elit. Inventore quaerat minus quo?</p>
              <button className="btn btn-outline-primary btn-lg text-center">Más información</button>
            </div>
            <div className="col-sm-12 col-md-6 mt-4">
              <img src={img2} alt="" className="img-thumbnail img-fluid" />
            </div>
          </div>
        </div>
      </section>
      <section id="llamado-accion">
        <div className="container">
          <div className="row mt-5">
            <div className="col-sm-12 col-md-12 text-center text-white">
              <h1 className="mt-5">Nuestros profesionales te esperan</h1>
              <p className="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde vitae similique beatae corrupti
                harum ducimus, laudantium ut voluptas deserunt iusto Lorem ipsum dolor sit amet consectetur adipisicing
                elit. Suscipit repellendus et rem!</p>
              <button className="btn btn-light btn-lg text-center mb-5">Mas información</button>
            </div>
          </div>
        </div>
      </section>

      <section id="servicios">
        <div className="container">
          <h1 className="text-center mt-5">Nuestros servicios</h1>
          <div className="row mt-5">
            <div className="col-sm-12 col-md-4 text-center">
              <h2 className="mt-5"><i className="fas fa-paint-brush"><br />Medicina general</i></h2>
              <p className="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae explicabo molestiae temporibus
                eos velit voluptates</p>
              <br />
              <button className="btn btn-info text-center mb-5">
                ver mas <i className="fas fa-arrow-circle-right"></i>
              </button>
            </div>
            <div className="col-sm-12 col-md-4 text-center">
              <h2 className="mt-5"><i className="fas fa-search-dollar"><br />Medicina especializada</i></h2>
              <p className="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae explicabo molestiae temporibus
                eos velit voluptates</p>
              <br />
              <button className="btn btn-info text-center mb-5">
                ver mas <i className="fas fa-arrow-circle-right"></i>
              </button>
            </div>
            <div className="col-sm-12 col-md-4 text-center">
              <h2 className="mt-5"><i className="fas fa-bullhorn"><br />Salud mental</i></h2>
              <p className="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae explicabo molestiae temporibus
                eos velit voluptates</p>
              <br />
              <button className="btn btn-info text-center mb-5">
                ver mas <i className="fas fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </div>
      </section>
      <section id="galeria">
        <div className="row mt-5 g-0">
          <div className="col-sm-12 col-md-3">
            <img src={imgGaleria1} alt="" className="img-fluid" />
          </div>
          <div className="col-sm-12 col-md-3">
            <img src={imgGaleria2} alt="" className="img-fluid" />
          </div>
          <div className="col-sm-12 col-md-3">
            <img src={imgGaleria3} alt="" className="img-fluid" />
          </div>
          <div className="col-sm-12 col-md-3">
            <img src={imgGaleria4} alt="" className="img-fluid" />
          </div>
        </div>
      </section>

      <section id="testimonios">
        <div className="container mb-5">
          <h1 className="text-center mt-5">testimonio</h1>
          <div className="row mt-5">
            <div className="col-sm-12 col-md-6 text-center">
              <div className="card">
                <div className="card-body">
                  <p className="mt-5">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur ut perferendis libero exercitationem
                    error.
                  </p>
                  <cite><strong>Fabio Cueva</strong></cite>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-md-6 text-center">
              <div className="card">
                <div className="card-body">
                  <p className="mt-5">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur ut perferendis libero exercitationem
                    error.
                  </p>
                  <cite><strong>Julian Reyes</strong></cite>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <footer>
        <section id="footer">
          <div className="container py-3">
            <div className="row my-5">
              <div className="col-sm-12 col-md-4 text-white">
                <h3 className="mt-5">Nosotros</h3>
                <p className="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor ex ut provident.</p>
                <p>@2022 jaior. Todos los derechos reservados</p>
              </div>
              <div className="col-sm-12 col-md-4 text-white">
                <h3 className="mt-5">Contactanos</h3>
                <p className="mt-5"><i className="fas fa-map-marker-alt"></i>carrera xx b # xx-xx xxxx/Colombia
                  <br /><br />
                  <i className="fas fa-phone"></i>
                  57 xxxxxx
                </p>
              </div>

              <div className="col-sm-12 col-md-4 text-white">
                <h3 className="mt-5">Suscríbete</h3>
                <br />
                <form>
                  <div className="form-group">
                    <label htmlFor="nombre">Nombre</label>
                    <input type="text" className="form-control" placeholder="Ingresa tu nombre" />
                  </div>

                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="email" className="form-control" placeholder="Ingresa tu email" />
                  </div>
                  <br />
                  <button type="submit" className="btn btn-primary">Suscríbete</button>
                </form>
              </div>
            </div>
          </div>
        </section>
      </footer>


    </main>

  )

}

export default Bienvenida;