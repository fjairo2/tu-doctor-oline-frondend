import { useEffect, useState } from "react";
import Estados from "../../enums/Estados";
import PacientesServicios from "../../servicios/PacientesServicios";

const ListadoPacientes = () => {

    const [listadoPacientes, setListadoPacientes] = useState([]);
    const [estado, setEstado] = useState(Estados.cargando);
    const [criterio, setCriterio] = useState("");
    const [idBorrar, setIdBorrar] = useState("");
    const [nombreBorrar, setNombreBorrar] = useState("");

    const cargarPacientes = async () => {
        try {
            const respuesta = await PacientesServicios.listarPacientes();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoPacientes(respuesta.data);
                setEstado(Estados.ok);
            } else {
                setEstado(Estados.vacio);
            }
        } catch (error) {
            setEstado(Estados.error);
        }
    }

    const buscarPacientes = async (event) => {
        event.preventDefault();
        try {
            const respuesta = await PacientesServicios.buscarPacientes(criterio);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoPacientes(respuesta.data);
                setEstado(Estados.ok);
            } else {
                setEstado(Estados.vacio);
            }
        } catch (error) {
            setEstado(Estados.error);
        }
    }

    const confirmarBorrado = (id, nombres) => {
        setIdBorrar(id);
        setNombreBorrar(nombres);
    }

    const borrarDoctor = async () => {
        try {
            await PacientesServicios.borrarPaciente(idBorrar);
            cargarPacientes();
        } catch (error) {

        }
    }

    useEffect(() => {
        cargarPacientes();
    }, [])

    const cambiarCriterio = (event) => {
        setCriterio(event.target.value);
    }

    return (
        <div className="container">
            <h3 className="mt-3">Lista de Pacientes</h3>
            <div className="row">
                <form action='' className="col-auto me-auto">
                    <input type="text" value={criterio} onChange={cambiarCriterio} id="criterio" name="criterio" />
                    <button id="buscar" name="buscar" onClick={buscarPacientes}>buscar</button>
                </form>
                <div className="col-auto">
                    <a className=" btn btn-sm btn-primary me-4" href={"/pacientes/form/"}>Nuevo registro</a>
                </div>
            </div>
            <table className="table table-sm">
                <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Documento</th>
                        <th>Dirección</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        estado === Estados.cargando ?
                            (<tr><td>
                                <div className="spinner-border text-secondary" role="status">
                                    <span className="sr-only"></span>
                                </div>
                            </td></tr>)
                            :
                            estado === Estados.error ?
                                (<tr><td>Ocurrió un error, intente más tarde</td></tr>)
                                :
                                estado === Estados.vacio ?
                                    (<div>No hay datos</div>)
                                    :
                                    listadoPacientes.map((paciente) => (
                                        <tr key={paciente._id}>
                                            <td>{paciente.nombres}</td>
                                            <td>{paciente.apellidos}</td>
                                            <td>{paciente.documento}</td>
                                            <td>{paciente.direccion}</td>
                                            <td>{paciente.telefono}</td>
                                            <td>{paciente.correo}</td>
                                            <td>
                                                <a className="btn btn-sm btn-primary me-2" href={"/pacientes/form/" + paciente._id}>Editar</a>
                                                <button onClick={() => { confirmarBorrado(paciente._id, paciente.nombres) }} className="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modalBorrado">Eliminar</button>
                                            </td>
                                        </tr>
                                    ))
                    }
                </tbody>
            </table>
            <div className="modal fade" id="modalBorrado" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">borrado de pacientes</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            Esta seguro de borrar el paciente {nombreBorrar}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-light" data-bs-dismiss="modal">Cancelar</button>
                            <button onClick={borrarDoctor} type="button" className="btn btn-danger" data-bs-dismiss="modal">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListadoPacientes;