import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import DoctoresServicios from "../../servicios/DoctoresServicios";

const FormDoctores = () => {
    const { id } = useParams();
    const navegar = useNavigate();

    const [nombres, setNombres] = useState("");
    const [apellidos, setApellidos] = useState("");
    const [documento, setDocumento] = useState("");
    const [especialidad, setEspecialidad] = useState("");
    const [telefono, setTelefono] = useState("");
    const [direccion, setDireccion] = useState("");
    const [correo, setCorreo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmar, setConfirmar] = useState("");
    const [mensaje, setMensaje] = useState("");
    const [titulo, setTitulo] = useState("");

    const guardarDoctor = async (event) => {
        event.preventDefault();

        if (password === confirmar) {
            try {
                const doctor = {
                    nombres: nombres,
                    apellidos: apellidos,
                    documento: documento,
                    especialidad: especialidad,
                    direccion: direccion,
                    telefono: telefono,
                    correo: correo,
                    password: password,
                    rol: "3"
                }
                console.log(doctor);
                if (id == null) {
                    await DoctoresServicios.guardarDoctor(doctor);
                    navegar("/");
                } else {
                    await DoctoresServicios.modificarDoctor(id, doctor);
                    navegar("/doctores");
                }
            } catch (error) {
                setMensaje("Ocurrió un error")
            }
        } else {
            setMensaje("Las contraseñas no coinciden");
        }
    }

    const cargarDoctor = async () => {
        try {
            if (id != null) {
                const respuesta = await DoctoresServicios.buscardoctor(id);
                if (respuesta.data != null) {
                    console.log(respuesta.data)
                    setNombres(respuesta.data.nombres);
                    setApellidos(respuesta.data.apellidos);
                    setDocumento(respuesta.data.documento);
                    setEspecialidad(respuesta.data.especialidad);
                    setDireccion(respuesta.data.direccion);
                    setTelefono(respuesta.data.telefono);
                    setCorreo(respuesta.data.correo);
                    setPassword(respuesta.data.password);
                    setConfirmar(respuesta.data.password);
                }
                setTitulo("Edición");
            } else {
                setTitulo("Registro");
            }
        } catch (error) {
            console.log("Ocurrió un error")

        }
    }
    const cancelar = () => {
        if (id != null) {
            navegar("/doctores");
        } else {
            navegar("/");
        }
    }
    useEffect(() => {
        cargarDoctor(id);
    }, [])

    const cambiarNombres = (event) => {
        setNombres(event.target.value);
    };
    const cambiarApellidos = (event) => {
        setApellidos(event.target.value);
    };
    const cambiarDocumento = (event) => {
        setDocumento(event.target.value);
    };
    const cambiarEspecialidad = (event) => {
        setEspecialidad(event.target.value);
    }
    const cambiarDireccion = (event) => {
        setDireccion(event.target.value);
    };
    const cambiarTelefono = (event) => {
        setTelefono(event.target.value);
    };
    const cambiarCorreo = (event) => {
        setCorreo(event.target.value);
    };
    const cambiarPassword = (event) => {
        setPassword(event.target.value);
    };
    const cambiarConfirmar = (event) => {
        setConfirmar(event.target.value);
    }

    return (
        <div className="container mt-3">
            <h3>{titulo} de doctores</h3>
            <form className="container" action="">
                <div className="row mt-5">
                    <div className="col-4">
                        <label htmlFor="nombres">Nombres *</label>
                        <input className="form-control" onChange={cambiarNombres} value={nombres} type="text" name="nombres" id="nombres" />
                    </div>
                    <div className="col-4">
                        <label htmlFor="apellidos">Apellidos *</label>
                        <input className="form-control" onChange={cambiarApellidos} value={apellidos} type="text" name="apellidos" id="apellidos" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <label htmlFor="documento">Ingrese documento*</label>
                        <input className="form-control" onChange={cambiarDocumento} readOnly={id != null ? true : false} value={documento} type="number" name="documento" id="documento" />
                    </div>
                    <div className="col-4">
                        <label htmlFor="especialidad">Especialidad *</label>
                        <input className="form-control" onChange={cambiarEspecialidad} value={especialidad} type="text" name="especialidad" id="especialidad" />
                    </div>
                </div>

                <div className="row">
                    <div className="col-4">
                        <label htmlFor="telefono">Telefono*</label>
                        <input className="form-control" onChange={cambiarTelefono} value={telefono} type="number " name="telefono" id="telefono" />
                    </div>
                    <div className="col-4">
                        <label htmlFor="correo">Ingrese correo electrónico *</label>
                        <input className="form-control" onChange={cambiarCorreo} value={correo} type="email" name="correo" id="correo" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-5">
                        <label htmlFor="direccion">dirección *</label>
                        <input className="form-control" onChange={cambiarDireccion} value={direccion} type="text" name="direccion" id="direccion" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <label htmlFor="password">password*</label>
                        <input className="form-control" onChange={cambiarPassword} value={password} type="password" name="password" id="password" />
                    </div>
                    <div className="col-4">
                        <label htmlFor="confirmar">validar password *</label>
                        <input className="form-control" onChange={cambiarConfirmar} value={confirmar} type="password" name="confirmar" id="confirmar" />
                    </div>
                </div>
                <div className="mt-3">
                    <button onClick={guardarDoctor} className="btn btn-primary me-2">Guardar</button>
                    <button onClick={cancelar} type="button" className="btn btn-secondary">Cancelar</button>
                    <div id="mensaje">{mensaje}</div>
                </div>
            </form>
        </div>
    )
}

export default FormDoctores;