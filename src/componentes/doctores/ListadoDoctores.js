import DoctoresServicios from "../../servicios/DoctoresServicios";
import Estados from "../../enums/Estados";
import { useEffect } from "react";
import { useState } from "react";
const ListadoDoctores = () => {

    const [ListadoDoctores, setListadoDoctores] = useState([]);
    const [estado, setEstado] = useState(Estados.cargando);
    const [criterio, setCriterio] = useState("");
    const [idBorrar, setIdBorrar] = useState("");
    const [nombreBorrar, setNombreBorrar] = useState("");

    const cargarDoctores = async () => {
        try {
            const respuesta = await DoctoresServicios.listarDoctores();
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoDoctores(respuesta.data);
                setEstado(Estados.ok);
            } else {
                setEstado(Estados.vacio);
            }
        } catch (error) {
            setEstado(Estados.error);
        }
    }

    const buscarDoctores = async (event) => {
        event.preventDefault();
        try {
            const respuesta = await DoctoresServicios.buscarDoctores(criterio);
            console.log(respuesta.data);
            if (respuesta.data.length > 0) {
                setListadoDoctores(respuesta.data);
                setEstado(Estados.ok);
            } else {
                setEstado(Estados.vacio);
            }
        } catch (error) {
            setEstado(Estados.error);
        }
    }

    const confirmarBorrado = (id, nombres) => {
        setIdBorrar(id);
        setNombreBorrar(nombres);
    }

    const borrarDoctor = async () => {
        try {
            await DoctoresServicios.borrarDoctor(idBorrar);
            cargarDoctores();
        } catch (error) {

        }
    }

    useEffect(() => {
        cargarDoctores();
    }, [])

    const cambiarCriterio = (event) => {
        setCriterio(event.target.value);
    }

    return (
        <div className="container">
            <h3 className="mt-3">Lista de Doctores</h3>
            <div class="row">
                <form action='' class="col-auto me-auto">
                    <input type="text" value={criterio} onChange={cambiarCriterio} id="criterio" name="criterio" />
                    <button id="buscar" name="buscar" onClick={buscarDoctores}>buscar</button>
                </form>
                <div className="col-auto">
                    <a className=" btn btn-sm btn-primary me-4" href={"/doctores/form/"}>Nuevo registro</a>
                </div>
            </div>
            <table className="table table-sm mt-3">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Documento</th>
                        <th>Especialidad</th>
                        <th>Dirección</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        estado === Estados.cargando ?
                            (<tr><td>
                                <div className="spinner-border text-secondary" role="status">
                                    <span className="sr-only"></span>
                                </div>
                            </td></tr>)
                            :
                            estado === Estados.error ?
                                (<div>No hay datos</div>)
                                :
                                estado === Estados.vacio ?
                                    (<div>No hay datos</div>)
                                    :
                                    ListadoDoctores.map((doctor) => (
                                        <tr key={doctor._id}>
                                            <td>{doctor.nombres}</td>
                                            <td>{doctor.apellidos}</td>
                                            <td>{doctor.documento}</td>
                                            <td>{doctor.especialidad}</td>
                                            <td>{doctor.direccion}</td>
                                            <td>{doctor.telefono}</td>
                                            <td>{doctor.correo}</td>
                                            <td>
                                                <a className="btn btn-sm btn-primary me-2" href={"/doctores/form/" + doctor._id}>Editar</a>
                                                <button onClick={() => { confirmarBorrado(doctor._id, doctor.nombres) }} className="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modalBorrado">Eliminar</button>
                                            </td>
                                        </tr>
                                    ))

                    }
                </tbody>
            </table>
            <div className="modal fade" id="modalBorrado" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">borrado de doctores</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            Esta seguro de borrar el doctor {nombreBorrar}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-light" data-bs-dismiss="modal">Cancelar</button>
                            <button onClick={borrarDoctor} type="button" className="btn btn-danger" data-bs-dismiss="modal">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListadoDoctores;
